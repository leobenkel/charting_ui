class CreateUser < ActiveRecord::Migration
  def change
    create_table :users do |t|

    	t.string :name
    	t.string :email
    	t.integer :points
    	t.datetime :last_connection
    	t.integer :setting_1
    	t.integer :setting_2
    	t.string :setting_3

    	t.timestamps null: false
    end
  end
end
