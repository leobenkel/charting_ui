

module ChartingUi
	require "charting_ui/engine"
	require "charting_ui/version"
	require "charting_ui/configuration"
	require "charting_ui/view_helpers"
	require "charting_ui/chartable"

  	class << self
		attr_accessor :configuration
	end
	def self.configuration
		@configuration ||= Configuration.new
	end
	def self.configure
		yield(configuration)
	end
end
