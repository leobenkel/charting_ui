module ChartingUi
	module Chartable
		extend ActiveSupport::Concern

		included do
		end

		module ClassMethods
			attr_accessor :charting_ui_available_methods

			def acts_as_chartable(*queries)
				cattr_accessor :charting_ui_available_methods
				self.charting_ui_available_methods = queries.map(&:to_s)
			end
		end
	end
end

ActiveRecord::Base.send :include, ChartingUi::Chartable
