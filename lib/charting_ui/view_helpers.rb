module ChartingUi
	module ViewHelpers

		def create_charting_area_hardcoded_scatter(collection, title: ,
			title_tag: ChartingUi.configuration.default_title_tag,
			title_class: "",
			x_dimension: nil,
			y_dimension: nil,
			axe_x_title: ,
			axe_y_title:,
			offset_left: ChartingUi.configuration.default_offset_left,
			offset_right: ChartingUi.configuration.default_offset_right,
			offset_top: ChartingUi.configuration.default_offset_top,
			offset_bottom: ChartingUi.configuration.default_offset_bottom,
			font_family: ChartingUi.configuration.default_font_family,
			font_size: ChartingUi.configuration.default_font_size,
			bordered: true
			)

			render partial: 'charting_ui/chart_content', locals: {
				_charting: {
					bordered: bordered,
					title:
					{
						tag: title_tag,
						content: title,
						class: title_class
					},
					data: {
						type: "scatter",
						collection: collection,
						offsets: {
							left: offset_left,
							right: offset_right,
							top: offset_top,
							bottom: offset_bottom
						},
						font: {
							family: font_family,
							size: font_size
						},
						axes: {
							x: {
								title: axe_x_title
							},
							y:{
								title: axe_y_title
							}
						}
					}
				}
			}
		end


		def create_charting_model_histogram(
			model_class_name: ,
			name_of_key: ,
			title: ,
			title_tag: ChartingUi.configuration.default_title_tag,
			title_class: "",
			offset_left: ChartingUi.configuration.default_offset_left,
			offset_right: ChartingUi.configuration.default_offset_right,
			offset_top: ChartingUi.configuration.default_offset_top,
			offset_bottom: ChartingUi.configuration.default_offset_bottom,
			font_family: ChartingUi.configuration.default_font_family,
			font_size: ChartingUi.configuration.default_font_size,
			axe_x_title: Array(name_of_key).map{|n| n.capitalize() }.join(", "),
			axe_y_title: t("charting_ui.Sum"),
			bordered: true
			)

			render partial: 'charting_ui/chart_content', locals: {
				_charting: {
					bordered: bordered,
					title:
					{
						tag: title_tag,
						content: title,
						class: title_class
					},
					data: {
						type: "histogram_model",
						model: {
							name: model_class_name,
							key: Array(name_of_key)
						},
						offsets: {
							left: offset_left,
							right: offset_right,
							top: offset_top,
							bottom: offset_bottom
						},
						font: {
							family: font_family,
							size: font_size
						},
						axes: {
							x: {
								title: axe_x_title
							},
							y:{
								title: axe_y_title
							}
						}
					}
				}
			}
		end
	end
end

ActionView::Base.send :include, ChartingUi::ViewHelpers
