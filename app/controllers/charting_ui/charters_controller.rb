require_dependency 'charting_ui/application_controller'

module ChartingUi
	class ChartersController < ApplicationController
		def fetch_data
			model = charter_params[:model_name].classify.constantize
			if model == nil then
				respond_to do |format|
					format.json { render :json => { :error => "#{t("charting_ui.Wrong_model")}: '#{model}'"}, :status => 422 }
				end
				return
			end

			key = JSON.parse(charter_params[:model_key])
			if model.charting_ui_available_methods == nil
				respond_to do |format|
					format.json { render :json => { :error => "#{t('charting_ui.Need_to_define_acts_as_chartable')}: '#{key}'"}, :status => 422 }
				end
				return
			end
			key.each do |k|
				if ! model.charting_ui_available_methods.include?(k)
					respond_to do |format|
						format.json { render :json => { :error => "#{t('charting_ui.Wrong_key')}: '#{k}'"}, :status => 422 }
					end
					return
				end
			end

			global_result = {}
			model.all.each do |item|
				key.each do |k|
					begin
						key_result = item.send(k)
						if is_date? key_result
							key_result = modify_date(key_result, charter_params[:date_scope])
						else
							key_result = { value: key_result, display: key_result }
						end

						global_result = create_new_block(key_result, global_result, key)

						global_result[key_result[:value]][:values][k][:value] += 1
						global_result[key_result[:value]][:values][k][:ids].push(item.id)
						global_result[key_result[:value]][:ids].push(item.id)
					rescue Exception => e
						respond_to do |format|
							format.json { render :json => { :error => "#{t('charting_ui.Unknow_key')}: '#{key}' | Error: #{e}"}, :status => 422 }
						end
						return
					end
				end
			end

			global_result.delete(2) if global_result.has_key? 2

			all_integer = true
			global_result.each{|k,v| all_integer = false unless k.is_a? Integer}

			if all_integer
				tmp = global_result.map{|k,v| v[:key]}
				min_value = tmp.min
				max_value = tmp.max
				min_value.upto(max_value) {|n| create_new_block({value:n , display: n}, global_result, key)}
			end

			global_result = global_result.map{|k, v| v }
			global_result.each { |item| item[:values] = item[:values].map{|k,v| v} }
			global_result = global_result.sort { |left, right| left[:key] <=> right[:key] }

			respond_to do |format|
				format.json { render :json => { :success => global_result}, :status => 200 }
			end
			return
		end



		private
		def charter_params
			params.permit(:date_scope, :model_name, :model_key)
			params.require(:model_key)
			params.require(:model_name)
			return params
		end

		def modify_date(value, scope = "all")
			case scope
			when "year"
				return { value: Time.mktime(value.year), display: "#{value.year}" }
			when "month"
				return { value: Time.mktime(value.year, value.month), display: "#{value.year}-#{value.month}" }
			when "day"
				return { value: Time.mktime(value.year, value.month, value.day), display: "#{value.year}-#{value.month}-#{value.day}" }
			else
				return { value: value, display: "#{value}" }
			end
		end

		def is_date?(key)
			return key.is_a?(DateTime) || key.is_a?(ActiveSupport::TimeWithZone)
		end

		def create_new_block(key, result, keys)
			if ! result.has_key? key[:value]

				type = ""
				if key[:value].is_a? Integer
					type = "integer"
				elsif is_date? key[:value]
					type = "date"
				else
					type = "string"
				end

				result[key[:value]] = {values: {}, ids: [], key: key[:display], key_type: type }
				keys.each do |kk|
					result[key[:value]][:values][kk] = {value: 0, ids: [], serie: kk}
				end
			end
			return result
		end
	end
end
