#= require ./charting_core

class window.ChartEngine_HardCoded_Scatter extends ChartEngine_Core

	#########################################
	############### SETUP ###################
	constructor: (block) ->
		super(block)

		console.log "Chart scatter initialized!"

	get_info_box_content: () ->
		return '<ul class="charting_ui_info_container">
				<li><span class="charting_ui_info_title charting_ui_info_x_title" >x:</span>&nbsp;<span class="charting_ui_info_value charting_ui_info_box_x"></span></li>
				<li><span class="charting_ui_info_title charting_ui_info_y_title" >y:</span>&nbsp;<span class="charting_ui_info_value charting_ui_info_box_y"></span></li>
			</ul>'

	load_source: () ->
		@source = @data.collection
		@min_x = 0
		@max_x = @source.length
		@min_y = Math.min.apply(null, @source)
		@max_y = Math.max.apply(null, @source)
		console.log("MinX: " + @min_x + ", MaxX: " + @max_x)
		console.log("MinY: " + @min_y + ", MaxY: " + @max_y)
		return true

	#########################################
	############ DRAW GLOBAL ################
	draw_chart: () ->
		points = []
		self = this
		$.each(@source, (i, p) ->
		       points.push(self.convert_unit_to_pixel(new Chart_Point(i, p)))
		)
		@draw_lines(points, @default_line_color)

	#########################################
	######### UPDATE INTERACTIVE ############
	update_user_interaction: (mouse_coordinate) ->
		value_x =  Math.round(@convert_pixel_to_unit_x(mouse_coordinate.x))
		if value_x >= @source.length
			return false
		value_y = @source[value_x]


		mouse_coordinate.x = @convert_unit_to_pixel_x(value_x)
		mouse_coordinate.y = @convert_unit_to_pixel_y(value_y)

		@draw_line_interactive(new Chart_Point(@offset_left, mouse_coordinate.y), #new Chart_Point(mouse_coordinate.x - @default_font_size, mouse_coordinate.y),
		                      new Chart_Point(@canvas_width - @offset_right, mouse_coordinate.y),#new Chart_Point(mouse_coordinate.x + @default_font_size, mouse_coordinate.y),
		                      @default_cross_color, 1
		                      )
		@draw_line_interactive(new Chart_Point(mouse_coordinate.x, @offset_top), #new Chart_Point(mouse_coordinate.x, mouse_coordinate.y - @default_font_size),
		                      new Chart_Point(mouse_coordinate.x, @canvas_height - @offset_bottom),#new Chart_Point(mouse_coordinate.x, mouse_coordinate.y + @default_font_size),
		                      @default_cross_color, 1
		                      )

		#@previous_drawn_region.push(@draw_circle(@ctx_interactive, @convert_unit_to_pixel(new Chart_Point(value_x, value_y)), 5))
		@draw_circle(@ctx_interactive, @convert_unit_to_pixel(new Chart_Point(value_x, value_y)), 5)

		@previous_drawn_region.push([
								0,0#mouse_coordinate.x - @default_font_size, mouse_coordinate.y - @default_font_size,
								@canvas_width, @canvas_height #mouse_coordinate.x + @default_font_size, mouse_coordinate.y + @default_font_size
								])
		return true

	update_info_box_content: (mouse_coordinate) ->
		@info_box.find('.charting_ui_info_box_x').text(@round_up(@convert_pixel_to_unit_x(mouse_coordinate.x)))
		@info_box.find('.charting_ui_info_box_y').text(@round_up(@convert_pixel_to_unit_y(mouse_coordinate.y)))

	#########################################
