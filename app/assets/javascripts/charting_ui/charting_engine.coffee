#= require ./charting_scatter
#= require ./charting_histogram

@charts = []

@create_new_canvas = (block,  is_mobile, type) ->
	chart = switch type
		when "scatter" then new ChartEngine_HardCoded_Scatter(block)
		when "histogram_model" then new ChartEngine_Histogram(block)
		else null

	if chart == null
		console.log "Unknown type: " + type
		return

	charts.push(chart)
	register_canvas_resize(is_mobile)
	return chart

@register_canvas_resize = (is_mobile) ->
	if is_mobile
		window.addEventListener("orientationchange",
			() ->
				setTimeout(() ->
							$.each(charts, (i, chart) ->
								chart.canvas_resize()
							)
				, 150)
		, false);
	else
		$(window).resize(
			() ->
				$.each(charts, (i, chart) ->
					chart.canvas_resize()
				)
		)
	$.each(charts, (i, chart) ->
		chart.canvas_resize()
	)
