#= require ./charting_point

class window.ChartEngine_Core

	############# VARIABLES #################
	Super_Verbose = false
	first_time_dimension: true

	canvas_element: null
	ctx: null
	ctx_interactive: null
	canvas_element_interactive: null
	info_box: null
	source: null
	min_x: null
	max_x: null
	min_y: null
	max_y: null

	indent_x: null
	indent_y: null

	axe_x_title: null
	axe_y_title: null

	offset_left: -1
	offset_right: -1
	offset_top: -1
	offset_bottom: -1

	canvas_width: null
	canvas_height: null

	default_font_size: -1
	default_font: null

	default_line_color: "#aaaaff"
	default_cross_color: "magenta"
	default_color: "black"
	default_background_color: "white"

	old_width: -1
	old_height: -1

	info_box_padding: 5

	previous_drawn_region: [0, 0, 0, 0]

	ready_to_run: false

	colors: []

	color_resource: [[240,163,255],[0,117,220],[153,63,0],[76,0,92],[25,25,25],[0,92,49],[43,206,72],[255,204,153],[128,128,128],[148,255,181],[143,124,0],[157,204,0],[194,0,136],[0,51,128],[255,164,5],[255,168,187],[66,102,0],[255,0,16],[94,241,242],[0,153,143],[224,255,102],[116,10,255],[153,0,0],[255,255,128],[255,255,0],[255,80,5]]

	#########################################
	############### SETUP ###################
	constructor: (block) ->
		console.log "---------STARTING NEW CHART----------"
		self = this
		_dom_element_fix = $(block).find('.charting_ui_chart_fix')
		_dom_element_interactive = 	$(block).find('.charting_ui_chart_interactive')
		_dom_info_box = $(block).find('.charting_ui_info_block')
		_data =	$(block).find('.charting_ui_data')


		@canvas_element = $(_dom_element_fix)
		@ctx = @canvas_element.get(0).getContext("2d")

		@canvas_element_interactive = $(_dom_element_interactive)
		@ctx_interactive = @canvas_element_interactive.get(0).getContext("2d")

		@info_box = $(_dom_info_box)

		@data = $(_data).data().data

		#@colors = [@default_line_color, "green", "red" ]
		$.each(@color_resource, (i, value) ->
			self.colors.push("rgb(" + value[0] + ", " + value[1] + ", " + value[2] + ")")
		)

		@axe_x_title = @data.axes.x.title
		@axe_y_title = @data.axes.y.title

		@offset_left = @data.offsets.left
		@offset_right = @data.offsets.right
		@offset_top = @data.offsets.top
		@offset_bottom = @data.offsets.bottom

		@default_font_size = @data.font.size
		@default_font = @data.font.family

		@info_box.empty().append($(@get_info_box_content()))

		@ready_to_run = @load_source()

		if @ready_to_run
			@canvas_resize()
			@linkMouseInteraction()
			console.log "Chart CORE initialized!"
		else
			@info_box.empty()
			$(_data).text("...Failed to load the chart...")
			$(_data).show()
			console.log "Failed to load chart"

	load_source: () ->
		console.log("'load_source' need to be implemented.")
		throw new Error("'load_source' need to be implemented.")

	get_info_box_content: () ->
		console.log("'get_info_box_content' need to be implemented.")
		throw new Error("'get_info_box_content' need to be implemented.")

	linkMouseInteraction: ->
		self = this
		@canvas_element_interactive.mouseup (e) ->
			self.handlemouseup(e)

		@canvas_element_interactive.mousedown (e) ->
			self.handlemousedown(e)

		@canvas_element_interactive.mousemove (e) ->
			self.handlemousemove(e)

		@canvas_element_interactive.mouseout (e) ->
			self.handlemouseout(e)

		@canvas_element_interactive.get(0).addEventListener("touchstart", @handlemousedown, false)
		@canvas_element_interactive.get(0).addEventListener("touchend", @handlemouseup, false)
		@canvas_element_interactive.get(0).addEventListener("touchcancel", @handlemouseup, false)
		@canvas_element_interactive.get(0).addEventListener("touchmove", @handlemousemove, false)

		return true

	#########################################
	############ DRAW GLOBAL ################
	draw_axes: () ->
		@draw_axe_x()
		@draw_axe_x_indent(@getIndentation())
		@draw_axe_y()
		@draw_axe_y_indent()
		@draw_axes_titles()

	redraw: () ->
		@clear()
		@before_redraw()
		@draw_chart()
		@draw_axes()

	clear: () ->
		@ctx.clearRect(0, 0, @canvas_width, @canvas_height)
		@ctx_interactive.clearRect(0, 0, @canvas_width, @canvas_height)

	draw_chart: () ->
		console.log("'draw_chart' need to be implemented.")
		throw new Error("'draw_chart' need to be implemented.")

	getIndentation: () ->
		return 10

	before_redraw: () ->

	#########################################
	############# DRAW AXES #################
	draw_axe_x: () ->
		@draw_line(
			@convert_unit_to_pixel(new Chart_Point(@min_x, @min_y)),
			@convert_unit_to_pixel(new Chart_Point(@max_x, @min_y))
		)

	draw_axe_x_indent: (indentation) ->
		delta = (@max_x - @min_x)
		indentation = Math.min(indentation, delta)
		@indent_x = delta / indentation * 1.0
		for i in [0..indentation]
			n = i * @indent_x + @min_x
			unit_y = @convert_pixel_to_unit_y(@canvas_height - @offset_bottom + 5)

			@draw_line(
				@convert_unit_to_pixel(new Chart_Point(n, @min_y)),
 				@convert_unit_to_pixel(new Chart_Point(n, unit_y))
			)

			x_pixel = @get_axe_text_x_pixel_position(n)
			y_pixel = @canvas_height - @offset_bottom + 5 + @default_font_size
			location = new Chart_Point(x_pixel, y_pixel)

			@draw_text(@get_axe_text_x_text(n), location)

	get_axe_text_x_pixel_position: (n) ->
		return @convert_unit_to_pixel_x(n)

	get_axe_text_x_text: (n) ->
		return n

	draw_axe_y_indent: (indentation = 10) ->
		delta = (@max_y - @min_y)
		indentation = Math.min(indentation, delta)
		@indent_y = delta / indentation * 1.0
		for i in [0..indentation]
			n = i * @indent_y + @min_y
			unit_x = @convert_pixel_to_unit_x(@offset_left - 5)

			@draw_line(
				@convert_unit_to_pixel(new Chart_Point(@min_x, n)),
 				@convert_unit_to_pixel(new Chart_Point(unit_x, n))
			)

			y_pixel = (@convert_unit_to_pixel_y(n) + (@default_font_size * 0.4))
			x_pixel = @offset_left - 7
			location = new Chart_Point(x_pixel, y_pixel)

			@draw_text(parseInt(n), location, "right")

	draw_axes_titles: () ->
		axe_title_font_size = 15

		# x
		self = this

		align = "left"
		total_width_text = @get_text_width(@axe_x_title, align, axe_title_font_size)
		extra_width = 0
		$.each(@axe_x_title.split(","),
			(i, title)->
				extra_width += self.draw_text(title, new Chart_Point(self.canvas_width / 2 - total_width_text / 2 + extra_width, self.canvas_height - axe_title_font_size * 0.5 ), align, axe_title_font_size, self.colors[i])
			)

		# y
		@draw_text(@axe_y_title, new Chart_Point(@offset_left, @offset_top - axe_title_font_size * 0.9 ), "center", axe_title_font_size)

	draw_axe_y: () ->
		@draw_line(
			@convert_unit_to_pixel(new Chart_Point(@min_x, @min_y)),
			@convert_unit_to_pixel(new Chart_Point(@min_x, @max_y))
		)

	#########################################
	############ DRAW BASICS ################
	draw_lines: (points, color = @default_color) ->
		previous_point = points[0]
		for i in [1..points.length - 1]
			@draw_line(previous_point, points[i], color)
			previous_point = points[i]
		return

	draw_line: (point1, point2, color = @default_color, thickness = 2) ->
		@draw_line_generic(@ctx, point1, point2, color, thickness)

	draw_line_interactive: (point1, point2, color = @default_color, thickness = 2) ->
		@draw_line_generic(@ctx_interactive, point1, point2, color, thickness)

	draw_line_generic: (ctx, point1, point2, color = @default_color, thickness = 2) ->
		ctx.beginPath()
		ctx.lineWidth = thickness
		ctx.fillStyle = color
		ctx.strokeStyle = color
		ctx.moveTo(point1.x, point1.y)
		ctx.lineTo(point2.x, point2.y)
		ctx.stroke()

		if Super_Verbose
			console.log("Draw line from " + point1.to_s() + " to " + point2.to_s())

	draw_text: (text, point, align = "center", font_size = @default_font_size, color = @default_color, font = @default_font) ->
		return @draw_text_generic(@ctx, text, point, align, font_size, color, font)

	draw_text_generic: (context, text, point, align = "center", font_size = @default_font_size, color = @default_color, font = @default_font) ->
		if text == null
			return
		context.fillStyle = color
		context.textAlign = align
		context.font = font_size + "px " + font
		context.fillText(text, point.x, point.y)
		if Super_Verbose
			console.log "Drawing text: '" + text + "' at " + point.to_s()
		return context.measureText(text).width

	get_text_width: (text, align = "center", font_size = @default_font_size, font = @default_font) ->
		@ctx.textAlign = align
		@ctx.font = font_size + "px " + font
		return @ctx.measureText(text).width

	draw_circle: (context, point, radius, fillcolor = @default_cross_color, tickness = 2, bordercolor = @default_color) ->
		context.beginPath()
		context.arc(point.x, point.y, radius, 0, 2 * Math.PI, false)
		context.fillStyle = fillcolor
		context.fill()
		context.lineWidth = tickness
		context.strokeStyle = bordercolor
		context.stroke()

		return [point.x - radius, point.y - radius, point.x + radius, point.y + radius]

	draw_rectangle: (point1, point2, fillcolor = @default_line_color, bordercolor = @default_background_color, thickness = 2) ->
		return @draw_rectangle_generic(@ctx, point1, point2, fillcolor, bordercolor, thickness)

	draw_rectangle_interactive: (point1, point2, fillcolor = @default_line_color, bordercolor = @default_background_color, thickness = 2) ->
		return @draw_rectangle_generic(@ctx_interactive, point1, point2, fillcolor, bordercolor, thickness)

	draw_rectangle_generic: (context, point1, point2, fillcolor = @default_line_color, bordercolor = @default_background_color, thickness = 2) ->
		context.beginPath()
		context.rect(Math.min(point1.x, point2.x), Math.min(point1.y, point2.y),
		             Math.abs(point2.x - point1.x), Math.abs(point1.y - point2.y)
		             )
		context.fillStyle = fillcolor
		context.fill()
		if thickness > 0
			context.lineWidth = thickness
			context.strokeStyle = bordercolor
			context.stroke()

		if Super_Verbose
			console.log("Draw rectangle from: " + point1.to_s() + ", to: " + point2.to_s())

		return [
			Math.min(point1.x, point2.x) - thickness,
			Math.min(point1.y, point2.y) - thickness,
			Math.max(point1.x, point2.x) + thickness,
			Math.max(point1.y, point2.y) + thickness
			]

	#########################################
	######### CONVERT COORDINATE ############
	convert_unit_to_pixel_x: (x) ->
		delta_x = @max_x - @min_x
		ratio_x = (@canvas_width - @offset_left - @offset_right) / delta_x * 1.0
		new_x = ((x - @min_x) * ratio_x) + @offset_left
		return new_x

	convert_pixel_to_unit_x: (x) ->
		delta_x = @max_x - @min_x
		ratio_x = delta_x / (@canvas_width - @offset_left - @offset_right) * 1.0
		new_x = ((x - @offset_left ) * ratio_x) + @min_x
		return new_x

	convert_unit_to_pixel_y: (y) ->
		delta_y = @max_y - @min_y
		ratio_y = (@canvas_height - @offset_bottom - @offset_top) / delta_y * 1.0
		new_y = @canvas_height - ((y - @min_y) * ratio_y) - @offset_bottom
		return new_y

	convert_pixel_to_unit_y: (y) ->
		delta_y = @max_y - @min_y
		ratio_y = delta_y / (@canvas_height - @offset_bottom - @offset_top) * 1.0
		new_y = ((@canvas_height - y  - @offset_bottom) * ratio_y) + @min_y
		return new_y

	convert_pixel_to_unit: (point) ->
		return new Chart_Point(@convert_pixel_to_unit_x(point.x), @convert_pixel_to_unit_y(point.y))

	convert_unit_to_pixel: (point) ->
		return new Chart_Point(@convert_unit_to_pixel_x(point.x), @convert_unit_to_pixel_y(point.y))

	test_convert_unit_pixel_unit: (x, y)->
		return @convert_pixel_to_unit(@convert_unit_to_pixel(new Chart_Point(x, y)))

	test_convert_unit_pixel_unit_x: (x)->
		return @convert_pixel_to_unit_x(@convert_unit_to_pixel_x(x))

	test_convert_unit_pixel_unit_y: (y)->
		return @convert_pixel_to_unit_y(@convert_unit_to_pixel_y(y))

	round_up: (value, decimal = 2)->
		result = Math.round(value * Math.pow(10, decimal)) / Math.pow(10, decimal)
		pieces = (result + "").split(".")
		decimal_part = pieces[1] || ""
		while decimal_part.length < decimal
			decimal_part += "0"

		return pieces[0] + "." + decimal_part

	#########################################
	############## RESIZE ###################
	canvas_resize: ()   ->
		if !@ready_to_run
			return false

		width = @getWindowWidth()
		height = width / 1.618
		height = Math.min(height, @getWindowHeight() / (1.618 * 2) )

		if width == @old_width && height == @old_height
			return

		if Super_Verbose || @first_time_dimension
			console.log 'w: ' + width
			console.log 'h: ' + height
			@first_time_dimension = false

		$(@canvas_element).attr('width', width)
		$(@canvas_element_interactive).attr('width', width)
		@canvas_width = width

		$(@canvas_element).attr('height', height)
		$(@canvas_element_interactive).attr('height', height)
		@canvas_height = height

		$(@canvas_element).parent().css('width', width)
		$(@canvas_element).parent().css('height', height)

		@redraw()
		@old_width = width
		@old_height = height
		return true

	getWindowWidth: () ->
		return $(@canvas_element).parent().parent().innerWidth()

	getWindowHeight: () ->
		return $(window).innerHeight()
	#########################################
	######### UPDATE INTERACTIVE ############
	updateInteractiveCanvas: (mouse_coordinate) ->
		@cleanUpInteractive()
		if(mouse_coordinate.x <= @offset_left || mouse_coordinate.x >= @canvas_width - @offset_right || mouse_coordinate.y <= @offset_top || mouse_coordinate.y >= @canvas_height - @offset_bottom)
			if !@info_box_always_on()
				@info_box.hide()
			else
				@info_box.show()
				@updateInfoBox(mouse_coordinate)
			return


		if @update_user_interaction(mouse_coordinate)
			@info_box.show()
			@updateInfoBox(mouse_coordinate)
		else
			@cleanUpInteractive()
			@info_box.hide()

	info_box_always_on: () ->
		return false

	update_user_interaction: (mouse_coordinate) ->
		console.log("'update_user_interaction' need to be implemented.")
		throw new Error("'update_user_interaction' need to be implemented.")

	updateInfoBox: (mouse_coordinate) ->
		@update_info_box_content(mouse_coordinate)
		@updateInfoBoxPosition(mouse_coordinate)

	updateInfoBoxPosition: (mouse_coordinate) ->
		if @canvas_width - mouse_coordinate.x - @offset_right <= @info_box.outerWidth()
			@info_box.css('left', (mouse_coordinate.x - @info_box.outerWidth() - @info_box_padding) + 'px')
		else
			@info_box.css('left', (mouse_coordinate.x + @info_box_padding) + 'px')

		if @canvas_height - mouse_coordinate.y - @offset_bottom <= @info_box.outerHeight()
			@info_box.css('top', (mouse_coordinate.y - @info_box.outerHeight() - @info_box_padding) + 'px')
		else
			@info_box.css('top', (mouse_coordinate.y + @info_box_padding) + 'px')

		@update_info_box_content(mouse_coordinate)

	update_info_box_content: (mouse_coordinate) ->
		console.log("'update_info_box_content' need to be implemented.")
		throw new Error("'update_info_box_content' need to be implemented.")

	cleanUpInteractive: () ->
		if @previous_drawn_region.length == 0
			return

		self = this
		$.each(@previous_drawn_region, (i, elem) ->
			self.ctx_interactive.clearRect(
				Math.min(self.canvas_width, Math.max(0, elem[0])),
				Math.min(self.canvas_height, Math.max(0, elem[1])),
				Math.min(self.canvas_width, Math.max(0, elem[2])),
				Math.min(self.canvas_height, Math.max(0, elem[3]))
				)
		)

		@previous_drawn_region = []

	#########################################
	############# USER INPUT ################
	extractCoordinate: (e) ->
		e.preventDefault()
		if e.touches?
			if e.touches.length == 0
				return null
			x = e.touches[0].pageX - e.touches[0].target.offsetParent.offsetLeft
			y = e.touches[0].pageY - e.touches[0].target.offsetParent.offsetTop
		else
			x = e.offsetX
			y = e.offsetY

		return {x: x, y: y}

	handlemousemove: (e) ->
		coordinate = @extractCoordinate(e)
		x = coordinate.x
		y = coordinate.y

		@updateInteractiveCanvas(coordinate)
		return false

	handlemouseup: (e) ->
		coordinate = @extractCoordinate(e)
		if(coordinate == null)
			x = last_x
			y = last_y
		else
			x = coordinate.x
			y = coordinate.y

		console.log "UP: " + x + ", " + y

		return false

	handlemousedown: (e) ->
		coordinate = @extractCoordinate(e)
		x = coordinate.x
		y = coordinate.y

		console.log "DOWN: " + x + ", " + y

		return false

	handlemouseout: (e) ->
		@cleanUpInteractive()
		@info_box.hide()

	#########################################
